﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.BackgroundAudio;
using System.Windows.Threading;
using System.Windows.Navigation;

namespace RadioAquaFelix
{
    public partial class MainPage : PhoneApplicationPage
    {
        DispatcherTimer _dt;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            InitializeStreaming();
        }

        public void InitializeStreaming()
        {
            progressBar.Visibility = Visibility.Visible;

            _dt = new DispatcherTimer();
            _dt.Interval = new TimeSpan(0, 0, 10); // 10 seconds
            _dt.Tick += new EventHandler(HideProgressBar);

            playPauseButton.IsChecked = true;
            playPauseButton.IsEnabled = false;
            playPauseButton.Opacity = 0.5;

            _dt.Start();

            AudioTrack track = BackgroundAudioPlayer.Instance.Track;
            track = new AudioTrack();
            track.BeginEdit();
            track.Source = new Uri("http://nr8.newradio.it:9175" +
                "/; stream.mp3", UriKind.Absolute); // WORKAROUND: this fixes problems with urls ending with port number
            track.EndEdit();
            BackgroundAudioPlayer.Instance.Track = track;
            BackgroundAudioPlayer.Instance.Play();
        }

        private void HideProgressBar(object sender, EventArgs e)
        {
            progressBar.Visibility = Visibility.Collapsed;
            playPauseButton.IsEnabled = true;
            playPauseButton.Opacity = 1;
            playPauseButton.IsChecked = true;
            _dt.Stop();
        }

        private void playPauseButton_Click(object sender, RoutedEventArgs e)
        {
            if (BackgroundAudioPlayer.Instance.PlayerState == PlayState.Playing)
            {
                BackgroundAudioPlayer.Instance.Stop();
                playPauseButton.IsChecked = false;
            }
            else
            {
                BackgroundAudioPlayer.Instance.Play();
                playPauseButton.IsChecked = true;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (BackgroundAudioPlayer.Instance.PlayerState == PlayState.Playing)
                playPauseButton.IsChecked = true;
            else
                playPauseButton.IsChecked = false;
        }

        private void ShowAbout(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/RadioAquafelixAbout.xaml", UriKind.Relative));
        }
    }
}
